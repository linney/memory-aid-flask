from flask import Flask, render_template, request
from memoryaid.MemoryAid import MemoryAid

app = Flask(__name__)
memory_aid = MemoryAid()


@app.route('/')
def get_memories():
    memories = memory_aid.getMemories()
    return render_template('list.html', memories=memories.values())


@app.route('/add')
def add_memory():
    memory_aid.addMemory(request.args.get('title'), request.args.get('value'))
    return 'Hey'

@app.route('/login')
def login():
    return render_template('form.html')

@app.route('/test-post', methods=['POST'])
def testPost():
    print(request.form['username'])
    print(request.form['password'])
    return login()


if __name__ == '__main__':
    app.run()
